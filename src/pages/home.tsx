import React, { useEffect } from "react";
import IPage from "../interfaces/page";
import logging from "../config/logging";
import { Link } from "react-router-dom";

const HomePage: React.FunctionComponent<IPage> = props => {
  useEffect(() => {
    logging.info(`Loading ${props.name}`);
  }, [props.name])

  return (
    <div>
      <p>This is the HOME page!</p>
      <Link to="/about">Go to the about page!</Link>
    </div>
  );
}
export default HomePage;